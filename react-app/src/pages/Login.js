import React, {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from	'../UserContext';
import {Redirect, useHistory} from 'react-router-dom';

export default function Login(){

	const history = useHistory();
	
	// consume the userContext object in the login page via useContext()
	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [loginButton, setLoginButton] = useState(false);
	
	useEffect( () => {
		if (email !== '' && password !== '' ) {
			setLoginButton(true);
	
		}else{

		setLoginButton(false);
	
		}
	}, [email, password])

// API integration with fetch
// we will add fetch in login page
	
	//fetch("url", {options})
	// url = the URL access from API
	// options = optional parameters: method, headers, authorization, etc.

	// without options, this is a simple GET request, downloading the contents of the url
	// process: The browser starts the request right away and returns a promise that the calling code should use to get the result
	// getting a response is usually a two-stage process

	//.then( response => response.json) // parse the response as JSON object
	//.then(data => {console.log(data)}) // process the result that we want to show




	function loginUser(e){

		 e.preventDefault();

		 fetch('https://morning-falls-34881.herokuapp.com/users/login',{

		 	method : 'POST',
		 	headers: {'Content-Type' : 'application/json'},
		 	body: JSON.stringify({
		 		email : email,
		 		password : password
		 	})

		 })
		 .then(response => response.json())
		 .then(data => {
		 		console.log(data)
		 		//  let's do the response in our data once we receive it

		 		if (data.accessToken !== undefined) {

		 			localStorage.setItem('accessToken', data.accessToken);
		 			setUser({accessToken: data.accessToken});

		 			Swal.fire({
					 	title: "Yaaaaaaaaaay!!",
						icon: "success",
						text: "Successfully logged-in"
					})


		 			// get user's details from our token
		 			fetch('https://morning-falls-34881.herokuapp.com/users/details', {

		 				headers : {
		 					Authorization: `Bearer ${data.accessToken}`
		 				}

		 			})
		 			.then(res => res.json())
		 			.then(data => {
		 				console.log(`user details`, data);
		 				//  we sill check if the user is Admin or not
		 				// if admin, we will redirect to the /courses
		 				// if not, redirect to homepage

		 				if (data[0].isAdmin === true) {

		 					localStorage.setItem('email', data[0].email);
		 					localStorage.setItem('isAdmin', data[0].isAdmin);

		 					setUser({
		 						email: data[0].email,
		 						isAdmin: data[0].isAdmin
		 					})

		 					// what router can we use to redirect the page to /courses if isAdmin is true
		 					history.push('/allActiveProducts');
		 				}else{
		 					history.push('/');
		 				}


		 			})


		 		}else{

		 			Swal.fire({
					 	title: "Ooooops!!",
						icon: "error",
						text: "Something went wrong. Check your credentials"
					})

		 		}

		 		setEmail('');
		 		setPassword('');


		 	})








		// Swal.fire({
		// 		 	title: "Yaaaaaaaaaay!!",
		// 			icon: "success",
		// 			text: "Successfully logged-in"
		// 		})
		// 	// localStorage allows us to save data within our browsers as strings
		// 	// setItem() method of the storage interface, when passed a key name value, will add that key to the given storage object or update that key's value of it already exists
		// 	// setItem() is used to store data in the localSotrage as strings
		// 	localStorage.setItem('email', email);
		// 	setUser({email: email}); // once the login form is submitted, the email will be given as a value to the user context
			
		// 	setEmail('');
		// 	setPassword('');

	} 

// let's update again the Login.js component to make it redirect to the homepage when it is detected that there is a user currently logged in.
	if (user.accessToken !== null) {
		return <Redirect to="/" />
		// if the user.email has a value, instead of showing the login component, go tothe route indicated in the "to" prop of Redirect component
	}

	return (

	<Form onSubmit={(e)=> loginUser(e)}>
		<h1>Login</h1>
		<Form.Group>
			<Form.Label>Email Address:</Form.Label>
			<Form.Control type="email" placeholder="Enter Email" value = {email} onChange={e => setEmail(e.target.value)} required />
			<Form.Text className="text-muted">
				We'll never share your email with anyone else
			</Form.Text>
		</Form.Group>

		<Form.Group>
			<Form.Label>Password:</Form.Label>
			<Form.Control type="Password" placeholder="Enter Password" value = {password} onChange={e => setPassword(e.target.value)}  required />
		</Form.Group>
		{loginButton ? <Button variant="primary" type="submit">Login</Button> 
		: 
			<Button variant="primary" type="submit" disabled>Login</Button> 
		}
		 
	</Form>
// end of form login
	)
}