import React, {useState, useEffect, useContext} from 'react';
import	{Form, Button} from 'react-bootstrap';
import UserContext from	'../UserContext';
import Swal from 'sweetalert2';
import {useHistory} from 'react-router-dom';


export default function AddProduct(){
	const history = useHistory();
	const {user} = useContext(UserContext);
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState(true);

	useEffect(()=>{

		if (name !== '' && description !== '' & price !== 0) {
			setIsActive(true)
		}else{
			setIsActive(false)
		} //end of if

	},[name, description, price])//end of use effect


	function addProduct(e){
		e.preventDefault();

		fetch('https://morning-falls-34881.herokuapp.com/products/addProduct', {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('accessToken')}`

			},
			body: JSON.stringify({
				name : name,
				description : description,
				price : price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data === true) {
				Swal.fire({
					title: 'Product Created',
					icon: 'success'
				})
				history.push('/allActiveProducts');
			}else{
				Swal.fire({
					title: 'Product Creation Failed',
					icon: 'error'
				})
			}
		})

		setName('');
		setDescription('');
		setPrice('');
	}



	return(
		<>
			<h1>Create Course</h1>
			<Form onSubmit={e => addProduct(e)}>
				<Form.Group>
					<Form.Label>Product Name:</Form.Label>
					<Form.Control 

						type="text"
						placeholder = "Enter Name of the Product"
						value={name}
						onChange={(e) => setName(e.target.value)}
						required

					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Product Description:</Form.Label>
					<Form.Control 

						type="text"
						placeholder = "Enter Description"
						value={description}
						onChange={(e) => setDescription(e.target.value)}
						required

					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Product Price:</Form.Label>
					<Form.Control 

						type="number"
						value={price}
						onChange={(e) => setPrice(e.target.value)}
						required

					/>
				</Form.Group>
				{ 
					isActive ?
					<Button type="submit" variant="primary">Submit</Button>
					:
					<Button type="submit" variant="primary" disabled>Submit</Button>	
				}
				
			</Form>

{/* end of return */}
		</>	
	)

}