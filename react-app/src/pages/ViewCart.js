
import React, {useContext, useEffect, useState} from 'react';
//import courseData from '../data/courses'; // to acquire the actual data that we want to display, describes all the courses records from the data folder
import {Table, Card, ListGroup, ListGroupItem} from 'react-bootstrap';
import CheckOutButton from '../components/CheckOutButton';
import {Link, NavLink} from 'react-router-dom';
import Nav from 'react-bootstrap/Nav'

export default function ViewCart(){
	
	const [cartList, setCartList] = useState([]);
	const [noData, setNoData] = useState('')


	useEffect(() => {
		
		fetch('https://morning-falls-34881.herokuapp.com/users/viewCart', {
			headers:{
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('accessToken')}`
			}


		})
			.then(res => res.json())
			.then(data => {
				console.log(data);
				
				if (data == '') {
					localStorage.setItem('data', false);
					console.log("nullnull")
					setNoData((data)=>{
						return(
							<>
								<Card className="m-3 text-center">
									<Card.Header as="h5">Your Cart </Card.Header>
									<Card.Body>
									 <Card.Title>
									    Cart is empty
									 </Card.Title>
									  <Card.Text>
									  	<Nav.Link as={NavLink} to="/allActiveProducts" >Go back to Products</Nav.Link>
									  </Card.Text>								    
									</Card.Body>
									
								</Card>
							</>
						)
					})
				}else{		
					localStorage.setItem('data', true);

					setCartList(data.map(cart => {
						return(
								<>
								<Card>
									<Card.Header as="h5">Order Reference #:  {cart._id} </Card.Header>
									<Card.Body>
									  <Card.Title>{cart.orderItems[0].productName}</Card.Title>
									  <Card.Text>
									    {cart.orderItems[0].description}
									  </Card.Text>								    
									</Card.Body>
									<ListGroup className="list-group-flush">
									  <ListGroupItem>Order Status: <span className="text-danger">{cart.orderStatus}</span></ListGroupItem>								    
									  <ListGroupItem>Total : {cart.total}</ListGroupItem>
									</ListGroup>
									<Card.Body>
										<CheckOutButton orderId={cart._id}/>
									</Card.Body>
									<Card.Footer className="text-muted">Date Ordered: {cart.orderDate}</Card.Footer>
								</Card>
								
								</>
							

						)

					}))
				}
			

			})
		


	},[])

		let getData = localStorage.getItem('data')
		
		if (getData == 'true') {
			return (
				
				<>
					<h1 className="text-center mb-5">Your Cart</h1>
					<Table striped bordered hover>
						{cartList}
					</Table>
					
				</>
			)

		}else {
			
			return(
			<>
				
				{noData}
				
			</>
			)
		}

		

	


}